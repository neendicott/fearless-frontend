import Nav from './Nav';
import AttendeesList from './AttendeeList';
import LocationForm from './LocationForm';
import { BrowserRouter, Route, Routes } from "react-router-dom"
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" elmenet={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
            <Route path="" element={<AttendeesList attendees={props.attendees} />} />
          </Route>
          <Route path ="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
        {/* <LocationForm/> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;