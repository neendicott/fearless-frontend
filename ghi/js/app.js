function createCard(name, description, pictureUrl,startDate,endDate,location) {
    return `
      <div class="card" style "column-gap":normal>
      <div class="shadow-lg p-3 mb-5 bg-body rounded"></div>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class = "card-footer">
        <small Class = "text-muted">${startDate} - ${endDate}</small>
        </div>
      </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        const cardsDiv = documnet.querySelector("div#cards");
        let columnNum = 0;

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const ends = details.conference.ends;

            const startDate = new Date(starts).toLocaleDateString();
            const endDate = new Date(ends).toLocaleDateString();
            const html = createCard(name, description, pictureUrl,startDate,endDate,);
            const columns = document.querySelector('.col');
            let column = columns[columnNum];
            columnNum += 1;
            if (columnNum > 2){
              columnNum = 0;
            }
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }
  
  });