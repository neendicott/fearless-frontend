window.addEventListener("DOMContentLoaded", async() => {
    const url = 'http://localhost:8000/api/states/';
  
    try{
      const response = await fetch(url)
  
      if (!response.ok){
        console.log("Something went wrong with the request")
      } else {
        const data = await response.json();
  
        const selectTag = document.getElementById("state");
        for (let state of data.states){
          const optionTag = document.createElement('option')
          optionTag.value = state.abbreviation
          optionTag.innerHTML = state.name
          selectTag.appendChild(optionTag)
  
        }
  
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async(event) => {
          event.preventDefault();
  
          const formData = new FormData(formTag);
          const json = JSON.stringify(Object.fromEntries(formData));
          const locationUrl = 'http://localhost:8000/api/locations/';
          const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(locationUrl, fetchConfig);
          if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
          }
          else{
            console.log("Something went wrong with the server")
          }
        });
        
      }
    } catch(e){
      console.log("error", e)
    }
  
  });